import React, { useMemo } from "react";
// import NativeSelect from '@material-ui/core/NativeSelect';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import "./style.scss";
import { makeStyles } from "@material-ui/core";
let selectIndex = 0;
const useStyles = makeStyles((theme) => {
    return{
        root: {
            width: "18%",
            border: (props) => `4px solid ${props.themeColor}`,
            "&.Mui-focused":{
                borderColor: "blue"
            }
        },
        select: {
            background: "transparent",
            color: (props) => props.themeColor,
            width: "100%",
            textTransform: "capitalize",
            boxSizing: "border-box",
            textAlign: "center"
        },
        icon: {
            color: (props) => props.themeColor
        }
    }
    
});
export default function DropDown({
    list,
    onChange,
    value,
    placeHolder,
    dropDownLabel,
    inputProps
}){
    const uniqueIndex =  useMemo(()=>++selectIndex,[]);
    const styles = useStyles({
        themeColor: "yellow"
    });
    const defaultInputProps = {
        id: `id-${uniqueIndex}`,
        name: `name-${uniqueIndex}`,
    }
    return(
        <div className="drop-down-holder">
            {dropDownLabel}
            
            <Select
                value={value} 
                onChange={onChange} 
                className={styles.root}
                displayEmpty
                inputProps={{...defaultInputProps,...inputProps}}
                classes={{
                    select: styles.select,
                    icon: styles.icon
                }}
                MenuProps={{
                    getContentAnchorEl: null,
                    anchorOrigin: {
                      vertical: "bottom",
                      horizontal: "left"
                    }
                }}
                >
                    {placeHolder ? <MenuItem value="" disabled>{placeHolder}</MenuItem> : null}
                {
                    list.map((item,index) => {
                        const {key, value} = item;
                        return <MenuItem key={index} value={key}>{value}</MenuItem>
                    })
                }
            </Select>
        </div>
        
    )
}